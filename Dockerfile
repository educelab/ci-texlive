FROM thomasweise/texlive:latest
MAINTAINER Seth Parker <c.seth.parker@uky.edu>

RUN apt-get clean &&\
    apt-get update &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    apt-get update &&\
    apt-get install -f -y \
      biber \
      latexdiff \
      texlive-publishers \
      texlive-fonts-extra \
      &&\
    /usr/bin/luaotfload-tool --update &&\
    apt-get clean &&\
    apt-get autoclean -y &&\
    apt-get autoremove -y &&\
    apt-get clean &&\
    rm -rf /tmp/* /var/tmp/* &&\
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/bin/__boot__.sh"]

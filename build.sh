#!/bin/bash

REPO=volcart/texlive
VERSION=5

docker build -t ${REPO}:${VERSION} -f Dockerfile .
